from src.xtender_api.preprocessing_types import UnprocessableCodePartsState
from src.xtender_api.preprocessing_utils import readbox



def test_readbox_simple():
    code_string = '[]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, '[', 0, ucp) == 1
    
def test_readbox_simple_reversed():
    code_string = '[]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, ']', 1, ucp, reverse=True) == 0
    
def test_readbox_spaced():
    code_string = '[         ]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, '[', 0, ucp) == 10
    
def test_readbox_spaced_reversed():
    code_string = '[         ]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, ']', 10, ucp, reverse=True) == 0
    
def test_readbox_simple_nested():
    code_string = '[[[[][]][]]]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, '[', 1, ucp) == 10

def test_readbox_simple_nested_reversed():
    code_string = '[[[[][]][]]]'
    ucp = UnprocessableCodePartsState.from_source(code_string)
    
    assert readbox(code_string, ']', 7, ucp, reverse=True) == 2