from src.xtender_api.str_utils import FstringToken, expect_after_spacing, find_string_literal_second_border, read_fstring_token, get_fstring_constant_regions



def test_read_fstring_token_simple():
    code_string = '"'
    res = read_fstring_token(code_string, 0)
    
    assert res == (FstringToken.DOUBLE_QUOTE, 1)


def test_read_fstring_token_simple_triple():
    code_string = '"""'
    res = read_fstring_token(code_string, 0)
    
    assert res == (FstringToken.TRIPLE_DOUBLE_QUOTE, 3)


def test_get_fstring_constant_regions_empty_single_quote():
    code_string = "f''"
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,2)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])



def test_get_fstring_constant_regions_empty_double_quote():
    code_string = 'f""'
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,2)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_empty_triple_single_quote():
    code_string = "f''''''"
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,6)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_empty_triple_double_quote():
    code_string = 'f""""""'
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,6)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_simple_triple_single_quote():
    code_string = ";f'''Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'''"
    
    res = get_fstring_constant_regions(code_string, 2)
    expected = [(1,98)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_ignore_double_curly_braces():
    code_string = "f'''a{{x}}b'''"
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,13)]
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])
    

def test_get_fstring_constant_regions_one_level_simple_var_interpolation():
    code_string = "f'''a{x}b'''"
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,5),(7,11)]
    
    print(res)
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_one_level_nested_fstring():
    code_string = """f'''a{ f"xxx" }b'''"""
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,5),(7,12),(14,18)]
    
    print(res)
    
    assert len(res) == len(expected)
    assert all([a == b for a, b in zip(res, expected)])


def test_get_fstring_constant_regions_two_level_nested_fstring():
    code_string = """f'''{f"{f'{123}'}"}'''"""
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0,4),(5,7),(8,10),(14,15),(16,17),(18,21)]
    
    assert res == expected


def test_get_fstring_constant_regions_nested_with_double_curly_braces():
    code_string = """f'''text{f"innertext{{inner{f'text'}text}}innertext"}text'''"""
    
    res = get_fstring_constant_regions(code_string, 1)
    expected = [(0, 8), (9, 27), (28, 34), (35, 51), (52, 59)]
    
    assert res == expected


def test_expect_after_spacing_arrow_func():
    code_string = 'f = () => { ... }'

    res = expect_after_spacing(code_string, ')', 6, reverse=True)
    expected = True

    assert res == expected


def test_expect_after_spacing_let():
    code_string = 'let \n {}'

    res_1 = expect_after_spacing(code_string, '{', 3)
    res_2 = expect_after_spacing(code_string, '}', 3)

    expected_1 = True
    expected_2 = False

    assert res_1 == expected_1
    assert res_2 == expected_2


def test_expect_after_spacing_string_end_left():
    code_string = '   '

    res = expect_after_spacing(code_string, 'NEVER', 1, reverse=True)
    expected = False

    assert res == expected


def test_expect_after_spacing_string_end_right():
    code_string = '   '

    res = expect_after_spacing(code_string, 'NEVER', 1)
    expected = False

    assert res == expected