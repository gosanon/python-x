from xtender_api.ast_utils import set_ast_parent_labels
from xtender_api.build_utils import BuildChain
from xtender_api.fs_utils import read_file_sync
from xtender_api.core_file_preprocessing import load_core
from xtender_api.logging import LOGGER_MODE, log_python_code
from xtender_api.preprocessing_types import PreprocessorState
from xtender_api.settings_provider import load_settings
from xtender_api.logging import log, warn, error, init
from xtender_api.code_preprocessing import *



SETTINGS = load_settings()

# set up logger
init(LOGGER_MODE[SETTINGS.logger.upper()])

# prepare core
core_library = load_core(SETTINGS.core)

# prepare target source file
log('Reading source code')
source: str = read_file_sync(SETTINGS.source)

final_code = (
    BuildChain(source)
        .run_string_operations(
            initially_preprocess_let,
            initially_preprocess_arrow_functions
        )
        
        .build_ast()
        
        .run_ast_operations(
            set_ast_parent_labels,
            initially_preprocess_multiline_strings,
            finally_preprocess_arrow_functions,
            finally_preprocess_let,
            preprocess_anonimous_objects,
            preprocess_core_dependencies
        )
        
        .prepare_code_for_postprocessing()
        
        .run_postprocessing_operations(
            preprocess_comments,
            finally_preprocess_multiline_strings
        )
        
        .get_postprocessing_result()
)

log_python_code(final_code)