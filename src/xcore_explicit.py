# PythonX Core | gitlab.com/z.brevnyakov/python-x

# Warning! PythonX Core imports are always added if px methods/functions are to be added 
#! PythonX Core imports !#

#! PythonX Core functions definitions !#
def eq(element):
    return lambda t: t == element

def AnonimousObject(**kwargs):
    class AnonimousObjectType(dict):
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)
      
        def __str__(self):
            return self.__dict__.__str__()
        
        def __repr__(self):
            return self.__dict__.__repr__()

        def __getitem__(self, item):
            return self.__dict__[item]
        
        def __setitem__(self, item, value):
            self.__dict__[item] = value

    return AnonimousObjectType(**kwargs)

def p(*args, **kwargs):
    from sys import stdout
    ostream = kwargs['stream'] if 'stream' in kwargs else stdout
    sep = kwargs['sep'] if 'sep' in kwargs else ' '
    end = kwargs['end'] if 'end' in kwargs else '\n'
    ostream.write(sep.join([str(_x) for _x in args]) + end)

def inp(*args, **kwargs): # TODO: add split=<bool> kwarg, add repeat=<int> kwarg
    from sys import stdin 
    def v(e, t):
        return type(e) == t

    istream = kwargs['stream'] if 'stream' in kwargs else stdin
    u_inp = istream.readline()[:-1]
    if (len(args) == 0):
        return u_inp
    else:
        a = args[0]

        if len(args) == 1 and (v(a, bool) and a or v(a, int) and a == 1):
            return Iterator(u_inp.split())
        elif v(a, str):
            return Iterator(u_inp.split(a))
        elif callable(a):
            return Iterator([a(x) for x in u_inp.split()])
        elif v(a, int) and a >= 1:
            if (len(args) == 1):
                return Iterator([u_inp] + [input() for x in range(args[0] - 1)])
            else:
                res = [args[1](x) for x in u_inp.split()]
                for _x in range(a - 1):
                    res.append(*inp(args[1]))
                return Iterator(res)

#! PythonX Main class definition !#
class Iterator:
    def __iter__(self):
        return iter(self.iterator)
    
    def __next__(self):
        return next(self.iterator)

    def __init__(self, *args, **kwargs):
        def is_iterable(obj):
            is_obj_iterable = True
            try:
                obj = iter(obj)
            except:
                is_obj_iterable = False
            return is_obj_iterable

        args_count = len(args)

        if args_count == 0:
            self.iterator = iter([])
        elif args_count == 1:
            [first_arg] = args
            first_arg_type = type(first_arg)

            if first_arg_type == type(self) or is_iterable(first_arg):
                self.iterator = iter(first_arg)
            elif first_arg_type == int:
                # 123 becomes (1, 2, 3)
                self.iterator = map(int, str(first_arg))
            elif first_arg_type == float:
                # 123.01 becomes ((1, 2, 3), (0, 1))
                self.iterator = map(lambda part: map(int, part), str(first_arg).split('.'))
            else:
                self.iterator = iter((first_arg,))
        else:
            self.iterator = iter(args)

    def xsplit(self, condition=None):
        from itertools import groupby
        return type(self)(map(lambda res: type(self)(res[1]),
                filter(lambda res: res[0],
                    groupby(self, lambda el: not condition(el)))))

    def map(self, func):
        return type(self)(map(func, self))
    
    def aggregate(self, func, starting_value):
        from itertools import accumulate
        return accumulate(self, func=func, initial=starting_value)

    def to(self, new_type, join_to_string=False, join_key=''):
        return new_type(self) if not(join_to_string) else (
            new_type(join_key.join(list(map(lambda x: str(x), self.iterator))))
        )
    
    def slice(self, _from, _to, _step=1):
        from itertools import islice
        return type(self)(islice(self, _from, _to, _step))

    def sort(self, key=None, reverse=0):
        return type(self)(sorted(self, key=key, reverse=reverse))

    def print_as_list(self, start='', end='\n'):
        #TODO: FIX MEMORY ISSUES (?) BY EL-AFTER-EL printing
        p(start + '[', end='')
        p(*self, sep=', ', end=']' + end)
        return self
    
    def join_and_print(self, joining_str=' ', **kwargs):
        p(joining_str.join([str(x) for x in self]), **kwargs)

    def print_all(self, **kwargs):
        p(*self, **kwargs)
        return self

    def filter(self, func):
        return type(self)(filter(func, self.iterator))

    def call(self, func):
        return func(self)

    def safe_call(self, func):
        func(self)
        return self

    def apply(self, func):
        return type(self)(func(self))

    def run(self):
        for el in self.iterator:
            pass

        return self

    def chain(self, another_iterator):
        from itertools import chain
        return type(self)(chain(self, another_iterator))
