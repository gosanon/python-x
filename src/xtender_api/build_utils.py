from typing import Callable, Self
from ast import Module, parse, unparse
from functools import reduce

from xtender_api.logging import error, warn, log
from xtender_api.preprocessing_types import PreprocessorState

class BuildChain:
    def __init__(self, source):
        self.preprocessor_state = PreprocessorState(source)
        self.string_operations_result = source
        self.ast_operations_result = None
        self.postprocessing_operations_result = None
    
    def string_do(self, func: Callable[[str, PreprocessorState], str]) -> Self:
        log('str_do', func.__name__)
        self.string_operations_result = func(self.string_operations_result, self.preprocessor_state)
        return self
    
    def run_string_operations(self, *functions: Callable[[str, PreprocessorState], str]) -> Self:
        return reduce(
            lambda _self, func: _self.string_do(func),
            functions,
            self)
    
    def build_ast(self) -> Self:
        self.ast_operations_result = parse(self.string_operations_result)
        return self
    
    def ast_do(self, func: Callable[[Module, PreprocessorState], Module]) -> Self:
        log('ast_do', func.__name__)
        if self.ast_operations_result is None:
            error('You need to build the AST before running ast operations.')
            exit(0)
            
        self.ast_operations_result = func(self.ast_operations_result, self.preprocessor_state)
        return self
    
    def run_ast_operations(self, *functions: Callable[[Module, PreprocessorState], Module]) -> Self:
        return reduce(
            lambda _self, func: _self.ast_do(func),
            functions,
            self)

    def prepare_code_for_postprocessing(self) -> Self:
        if self.ast_operations_result is None:
            warn('Whole AST operations phase was skipped. This might leave left syntax preprocessing unfinished.')
            self.postprocessing_operations_result = self.string_operations_result
            return self
            
        self.postprocessing_operations_result = unparse(self.ast_operations_result)
        return self

    def get_postprocessing_result(self):
        if self.postprocessing_operations_result is None:
            error('You need to at least prepare code for postprocessing before fetching postprocess result.')
            exit(0)
        
        return self.postprocessing_operations_result
    
    def postprocess_do(self, func: Callable[[str, PreprocessorState], str]) -> Self:
        if self.postprocessing_operations_result is None:
            error('You need to prepare the AST for postprocessing or skip AST phase before running postprocessing operations.')
            exit(0)
            
        self.postprocessing_operations_result = func(self.postprocessing_operations_result, self.preprocessor_state)
        return self
    
    def run_postprocessing_operations(self, *functions: Callable[[str, PreprocessorState], str]) -> Self:
        return reduce(
            lambda _self, func: _self.postprocess_do(func),
            functions,
            self)
