import importlib.util


def import_module(module_name, module_path):
    spec = importlib.util.spec_from_file_location(module_name, module_path)
    
    if (spec is None):
        raise FileNotFoundError(f"Module {module_name} not found at {module_path}")
    
    module = importlib.util.module_from_spec(spec)
    
    if (spec.loader is None):
        raise ImportError(f"Module {module_name} is not a package")
    
    spec.loader.exec_module(module)
    
    return module
