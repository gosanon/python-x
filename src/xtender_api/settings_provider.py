import argparse
from pathlib import Path
from os import path

from xtender_api.module_utils import import_module
from xtender_api.logging import log


PX_FOLDER = Path(__file__).parent.parent.resolve()


def load_settings():
    parser = argparse.ArgumentParser(
        prog = 'px',
        description = 'PythonX to Python compiler',
        epilog = 'https://gitlab.com/gosanon/python-x')

    parser.add_argument(
        'source',
        type=str,
        help='Path to a file to compile')
    
    parser.add_argument(
        '-c',
        '--config',
        metavar='PATH_TO_CONFIG',
        type=str,
        help='Path to config')
    
    parser.add_argument(
        '--header',
        action='store_true',
        default=False,
        help='Option to write "#!/usr/bin/python" to first line of the file (default is False)')
    
    parser.add_argument(
        '--comment',
        type=str,
        default='Generated by XTender for PythonX',
        help='Comment added to the beginning of the compiled file')
    
    parser.add_argument(
        '--core',
        metavar='PATH_TO_CORE',
        type=str,
        default=path.join(PX_FOLDER, 'xcore.py'),
        help='Path to core library file (absolute or relative to current working directory, default is xcore.py near xtender.py)')
    
    parser.add_argument(
        '--autopep8',
        action='store_true',
        help='Apply autopep8 after compilation (default is False)')
    
    parser.add_argument(
        '--letvar',
        action='store_true',
        help='Option to generate `let` intermediate variables (slightly better readability, '
            'but will violate pep8 naming recommendations, default is False)')
    
    parser.add_argument(
        '--letpar',
        metavar='LET_PARAMETER_NAME',
        type=str,
        default='field',
        help='Lambda parameter name to use in `let` `map`s')
    
    parser.add_argument(
        '--postfix',
        action='store_true',
        help='Option to add `.compiled.py` at resulting filename name end (default is False)')
    
    parser.add_argument(
        '--pxname',
        metavar='PX_CLASS_NAME',
        type=str,
        default='px',
        help='Main class name (px/Iterator/...), default is "px"')
    
    parser.add_argument(
        '--oname',
        metavar='O_CLASS_NAME',
        type=str,
        default='o',
        help='Anonimous object class name (o/AnonimousObject/...; default is "o")')
    
    parser.add_argument(
        '-r',
        '--run',
        action='store_true',
        default=True,
        help='Run file in the same process after compilation (default is True)')
    
    parser.add_argument(
        '-l',
        '--logger',
        choices=['verbose', 'default', 'silent'],
        default='default',
        help='Logger verbosity mode (default is "default")')
    
    args = parser.parse_args()
    
    if (args.config is not None):
        if not path.exists(args.config):
            raise FileNotFoundError(f'Config file "{args.config}" does not exist')
        else:
            config_path = path.abspath(args.config)
            config_module = import_module('config', config_path)
            
            if not hasattr(config_module, 'SETTINGS'):
                raise AttributeError(f'Config file "{config_path}" does not contain SETTINGS visible export')
            
            settings = getattr(config_module, 'SETTINGS')
            
            for setting in settings:
                if not hasattr(args, setting):
                    raise AttributeError(f'Parameter "{setting}" does not exist')
                
                setattr(args, setting, settings[setting])
    
    return args
