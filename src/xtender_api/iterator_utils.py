from itertools import chain

class Iterator:
    def __iter__(self):
        return iter(self.iterator)
    
    def __next__(self):
        return next(self.iterator)

    def __init__(self, *args, **kwargs):
        def is_iterable(e):
            ii = True
            try:
                e = iter(e)
            except:
                ii = False
            return ii

        args_count = len(args)

        if args_count == 0:
            self.iterator = iter([])
        elif args_count == 1:
            [first_arg] = args
            first_arg_type = type(first_arg)

            if first_arg_type == Iterator or is_iterable(first_arg):
                self.iterator = iter(first_arg)
            elif first_arg_type == int:
                # 123 -> (1, 2, 3)
                self.iterator = map(int, str(first_arg))
            elif first_arg_type == float:
                # 123.01 -> ((1, 2, 3), (0, 1))
                self.iterator = map(lambda part: map(int, part), str(first_arg).split('.'))
            else:
                self.iterator = iter((first_arg,))
        else:
            self.iterator = iter(args)

    def xsplit(self, condition=lambda _: True):
        from itertools import groupby
        return Iterator(map(lambda res: Iterator(res[1]),
                filter(lambda res: res[0],
                    groupby(self, lambda el: not condition(el)))))

    def map(self, func):
        return Iterator(map(func, self))
    
    def aggregate(self, func, starting_value):
        from itertools import accumulate
        return accumulate(self, func=func, initial=starting_value)

    def to(self, new_type, join_to_string=False):
        return new_type(self) if not(join_to_string) else (
            new_type(''.join(list(map(lambda x: str(x), self.iterator))))
        )
    
    def slice(self, _from, _to, _step=1):
        from itertools import islice
        return Iterator(islice(self, _from, _to, _step))

    def sort(self, key=None, reverse=None):
        return Iterator(sorted(self, key=key, reverse=reverse)) #type:ignore

    def print_as_list(self, start='', end='\n'):
        print(start + '[', end='')
        print(*self, sep=', ', end=']' + end)
        return self
    
    def join_and_print(self, joining_str=' ', **kwargs):
        print(joining_str.join([str(x) for x in self]), **kwargs)

    def print_all(self, **kwargs):
        print(*self, **kwargs)
        return self

    def filter(self, func):
        return Iterator(filter(func, self.iterator))

    def call(self, func):
        return func(self)

    def safe_call(self, func):
        func(self)
        return self

    def apply(self, func):
        return Iterator(func(self))

    def run(self):
        for _ in self.iterator:
            pass

        return self

    def chain(self, another_iterator):
        return Iterator(chain(self, another_iterator))