def read_file_sync(path):
    with open(path, 'r', encoding='utf-8') as f:
        return f.read()