from ast import Module
from typing import Literal



class IntrafileDependecy:
    def __init__(self, type: Literal['function', 'class', 'variable'], name):
        self.type = type
        self.name = name
        
    def __repr__(self):
        return f"<{self.type} '{self.name}'>"


class Definition:
    def __init__(self, name: str, detected_intrafile_dependencies: list[IntrafileDependecy]):
        self.name: str = name
        self.detected_intrafile_dependencies: list[IntrafileDependecy] = detected_intrafile_dependencies

    def __repr__(self):
        return f"<Definition '{self.name}': {self.detected_intrafile_dependencies}>"


class CoreLibrary:
    def __init__(self, ast: Module, functions: list[Definition], classes: list[Definition], variables: list[Definition]):
        self.ast = ast
        self.functions = functions # top-level only
        self.classes = classes     # top-level only
        self.variables = variables # top-level only