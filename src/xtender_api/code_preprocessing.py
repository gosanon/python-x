from ast import Module
import json
from logging import warn
from queue import Queue
from typing import List
from xtender_api.preprocessing_utils import arrow_function_name_generator, generic_unsafe_readbox, unsafe_readword, validate_preprocess_operation, unsafe_readbox, readbox
from xtender_api.str_utils import expect_after_spacing, generic_expect_after_spacing, skip_spacing
from xtender_api.logging import error, log, log_python_code, print_highlighted_string_regions
from xtender_api.preprocessing_types import ArrowFunctionDefinition, PreprocessorState, UnprocessableCodePartsState



# String stage

def initially_preprocess_let(source: str, preprocessor_state: PreprocessorState) -> str:
    starting_index = source.find('let')
    
    if starting_index == -1:
        return source
    
    preprocessed_code_cache = ''
    last_touched_code_piece_end_index = 0
    current_index = starting_index
    while current_index != -1:
        # skip commented code and string literals
        if not validate_preprocess_operation(preprocessor_state.unprocessable_code_parts, current_index):
            current_index = source.find('let', current_index + 3)
            continue

        # validate `let` syntax [`let {` part]
        if not expect_after_spacing(source, '{', current_index + 3):
            # syntax error or code like "someletvar = ..."
            current_index = source.find('let', current_index + 3)
            continue
        
        possible_varnames_block_start_index = source.find('{', current_index)
        varnames_block_end_index = unsafe_readbox(source, '{', possible_varnames_block_start_index)

        if varnames_block_end_index == -1:
            error('Failed to find eclosing "}" for `let` statement.')
            exit(0)

        # validate `let` syntax [`} =` part]
        if not expect_after_spacing(source, '=', varnames_block_end_index + 1):
            error('"}" was not followed by "=" in `let` statement.')
            exit(0)

        assign_char_index = source.find('=', varnames_block_end_index)
        # will be 100% found because of the line above
        
        # code until `let` keyword
        preprocessed_code_cache += source[last_touched_code_piece_end_index:current_index]
        
        # So we want to change the representation of this statement
        # Before: str[before] + "let { a, b } = ..." + str[after]
        # After: str[before] + "(a, b, _pythonx_let) = ..." + str[after] # formatting may differ
            
        args_substr = source[possible_varnames_block_start_index + 1:varnames_block_end_index].strip()
        temp_assign_statement_source = f'({ args_substr }, _pythonx_let)'
        
        preprocessed_code_cache += temp_assign_statement_source
        
        last_touched_code_piece_end_index = assign_char_index
        current_index = last_touched_code_piece_end_index
        
        if not preprocessor_state.let_expressions_detected:
            preprocessor_state.let_expressions_detected = True
    
    return preprocessed_code_cache + source[last_touched_code_piece_end_index:]


def initially_preprocess_arrow_functions(
                    source: str,
                    preprocessor_state: PreprocessorState) -> str:

    name_generator = arrow_function_name_generator()

    class AFPreprocessedState:
        def __init__(
                self,
                updated_source: str,
                extracted_defs: List[ArrowFunctionDefinition],
                ucp: UnprocessableCodePartsState):
            self.updated_source = updated_source
            self.extracted_defs = extracted_defs
            self.ucp = ucp
        
    
    def preprocess(source, af_definitions, ucp: UnprocessableCodePartsState):
        result_state = AFPreprocessedState(source, [], ucp)
        
        starting_index = source.find('=>')

        if starting_index == -1:
            return result_state

        current_index = starting_index
        while current_index != -1:
            source = result_state.updated_source

            # skip commented code and string literals
            if not validate_preprocess_operation(ucp, current_index):
                current_index = source.find('=>', current_index + 2)
                continue

            if not preprocessor_state.arrow_functions_detected:
                preprocessor_state.arrow_functions_detected = True

            # try to define arguments type (long / short)
            matches_long_args_pattern = expect_after_spacing(source, ')', current_index - 1, reverse=True)
            
            args_first_char_index = -1
            args_last_char_index = skip_spacing(source, current_index - 1, reverse=True)
            
            if matches_long_args_pattern:
                # we do not try to parse lambdas passed as default arguments values
                # because params are always presented before `=>` keyword
                # which means that the lambda passed as a default value would be
                # found and parsed by this cycle before the outer one.

                args_first_char_index = readbox(source, ')', args_last_char_index, ucp, reverse=True)

            if not matches_long_args_pattern:
                # superficially validate syntax
                if not generic_expect_after_spacing(
                        source,
                        lambda char:
                            char.isalpha()
                            or char.isdigit()
                            or char == '_',
                        current_index - 1,
                        reverse=True):
                    error('Cannot parse arguments of an arrow function:'
                        'left side of `=>` must end with ")" or an alphabetic character / digit / "_".')
                    # actual_char_ix = skip_spacing(source, current_index - 1, reverse=)
                    # error(f'Instead got: "{char}"')
                    exit(0)
                
                args_first_char_index = unsafe_readword(
                    source,
                    args_last_char_index,
                    reverse=True
                )

            # TODO
            # 1 long body vs short body
            # 2 if body is short, we don't care about nested arrow functions
            # 3 if body is long, we should find it's end and handle nested arrow functions if those exist
            
            # if body is short, add "lambda " before args, ": " after args instead of "=>". Possibly, should recalculate UCP.
            # if body is long, we should replace whole expression from first arg char (or "(") to "}" with a new generic name,
            # then recalculate UCP, then process nested arrow functions.

            matches_long_body_pattern = expect_after_spacing(source, '{', current_index + 2)

            args_first_char_index_shift = 0
            args_last_char_index_shift = 0
            
            if source[args_first_char_index] == '(':
                args_first_char_index_shift += 1
                args_last_char_index_shift -= 1
            
            no_parentheses_args_string = \
                source[
                    args_first_char_index + args_first_char_index_shift
                    : args_last_char_index + 1 + args_last_char_index_shift].strip()
        
            if not matches_long_body_pattern: 
                any_args = no_parentheses_args_string.strip() != ''
                
                source_before_af = source[:args_first_char_index]
                lambda_kw = 'lambda'
                after_lambda_kw_spacing = ' ' if any_args else ''
                source_after_af = source[current_index + 2:]
                
                insertion = (
                    lambda_kw
                    + after_lambda_kw_spacing
                    + no_parentheses_args_string.strip()
                    + ':'
                )
                
                print_highlighted_string_regions(source, [(current_index, current_index + 1)], title='Before')
                log(['mod left', args_first_char_index, source[args_first_char_index]])
                log(['mod right old', current_index + 1, source[current_index + 1]])
                
                print_highlighted_string_regions(source, [( args_first_char_index, current_index + 1 )], title='Args UCP area')
                
                args_default_values_ucps = ucp \
                    .get_unprocessable_code_parts_within_borders(
                        args_first_char_index, current_index + 1)
                
                args_ucp_shift = len('lambda') # to be verified
                
                ucps_for_replacement = [ (left + args_ucp_shift, right + args_ucp_shift)
                                        for left, right in args_default_values_ucps]
                
                source = source_before_af + insertion + source_after_af
                
                log(['mod right new', args_first_char_index + len(insertion) - 1, source[args_first_char_index + len(insertion) - 1]])
                
                print_highlighted_string_regions(source,
                    [(args_first_char_index, args_first_char_index + len('lambda') - 1)], title='After')
                
                modified_region_left_border = args_first_char_index
                modified_region_old_right_border = current_index + 1
                modified_region_new_right_border = args_first_char_index + len(insertion)
                
                log(['replacements', ucps_for_replacement])

                print_highlighted_string_regions(source, ucp.parts, title='UCP before lambda recalc')

                log(['left, right_old, right_new'])
                log([modified_region_left_border, modified_region_old_right_border, modified_region_new_right_border])

                log('parts before')
                log(ucp.parts)
                ucp.recalculate_unprocessable_code_parts_on_code_change(
                    modified_region_left_border,
                    modified_region_old_right_border,
                    modified_region_new_right_border,
                    ucps_for_replacement)
                
                log('parts after')
                log(ucp.parts)
                print_highlighted_string_regions(source, ucp.parts, title='UCP after lambda recalc')

            elif matches_long_body_pattern:
                if not preprocessor_state.long_body_arrow_functions_detected:
                    preprocessor_state.long_body_arrow_functions_detected = True
                
                name = next(name_generator)
                
                # Parse body start and body end
                body_start = skip_spacing(source, current_index + 2)
                body_end = readbox(source, '{', body_start, ucp)
                
                # + 1 to include the "}"
                expression_len = (body_end + 1) - args_first_char_index
                
                # Do we really need args ucp?
                af_body_ucp = ucp.get_unprocessable_code_parts_within_borders(body_start, body_end)
                af_body_source = source[body_start + 1:body_end]
                
                original_len = len(af_body_source)
                
                af_body_source = af_body_source.lstrip()
                left_strip_len = len(af_body_source)

                af_body_source = af_body_source.rstrip()
                
                left_strip_diff = original_len - left_strip_len
                # Right strip diff seems to be useless

                af_body_ucp = [ (
                                    left - (body_start + 1 + left_strip_diff),
                                    right - (body_start + 1 + left_strip_diff)
                                ) for left, right in af_body_ucp ]

                af_def = ArrowFunctionDefinition(
                    name, no_parentheses_args_string, af_body_source, af_body_ucp)

                af_definitions[name] = af_def

                log('logs below: long body ucp recalc')
                log(['left', args_first_char_index, source[args_first_char_index]])
                log(['right old', body_end, source[body_end]])

                source = source[:args_first_char_index] + name + source[body_end + 1:]

                ucp_shift = len(name) - expression_len
                
                log(['right new', body_end+ucp_shift, source[body_end+ucp_shift]])
                
                # Obviously, no UCP for replacement required
                ucp.recalculate_unprocessable_code_parts_on_code_change(
                    args_first_char_index - 1, body_end, body_end + ucp_shift)
                
                print_highlighted_string_regions(
                    af_definitions[name].body_str,
                    af_definitions[name].ucp.parts,
                    title=f'{name} function body'
                )
                
                print_highlighted_string_regions(source, ucp.parts, title='Main code')

                result_state.extracted_defs.append(af_def)
            
            current_index = source.find('=>')

            result_state.updated_source = source
        
            
        return result_state
        
    af_to_process_queue: Queue[ArrowFunctionDefinition] = Queue()
    
    updated_state = preprocess(
        source,
        preprocessor_state.arrow_functions_definitions,
        preprocessor_state.unprocessable_code_parts)
    
    source = updated_state.updated_source
    first_level_af_defs = updated_state.extracted_defs
    preprocessor_state.top_level_contains_definitions_of = first_level_af_defs
    
    [ af_to_process_queue.put(d) for d in first_level_af_defs ]

    while not af_to_process_queue.empty():
        print('next iter')
        current_func = af_to_process_queue.get()

        warn(current_func.name)
        warn(current_func.body_str)
        warn(current_func.contains_definitions)
        
        func_updated_state = preprocess(
            current_func.body_str,
            preprocessor_state.arrow_functions_definitions,
            current_func.ucp)
        
        current_func.body_str = func_updated_state.updated_source
        current_func.ucp = func_updated_state.ucp

        [ af_to_process_queue.put(d) for d in func_updated_state.extracted_defs ]

        preprocessor_state.arrow_functions_definitions[current_func.name].contains_definitions = \
            func_updated_state.extracted_defs

    print('!!! END OF i_p_a_f !!!')
        
    return source


# AST stage

def initially_preprocess_multiline_strings(tree: Module, preprocessor_state: PreprocessorState) -> Module:
    return tree


def finally_preprocess_arrow_functions(tree: Module, preprocessor_state: PreprocessorState) -> Module:
    if not preprocessor_state.long_body_arrow_functions_detected:
        return tree
    
    

    return tree


def finally_preprocess_let(tree: Module, preprocessor_state: PreprocessorState) -> Module:
    if not preprocessor_state.let_expressions_detected:
        return tree
    
    return tree


def preprocess_anonimous_objects(tree: Module, preprocessor_state: PreprocessorState) -> Module:
    return tree


def preprocess_core_dependencies(tree: Module, preprocessor_state: PreprocessorState) -> Module:
    return tree


# Postprocessing stage

def preprocess_comments(source: str, preprocessor_state: PreprocessorState) -> str:
    return source


def finally_preprocess_multiline_strings(source: str, preprocessor_state: PreprocessorState) -> str:
    return source
