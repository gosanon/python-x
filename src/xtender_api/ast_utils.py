import ast
from xtender_api.preprocessing_types import PreprocessorState

from xtender_api.core_library_types import IntrafileDependecy, CoreLibrary



class KnownDependenciesCollector(ast.NodeVisitor):
    def __init__(self, known_dependencies: dict[str, IntrafileDependecy]):
        self.found_dependencies: set[IntrafileDependecy] = set()
        
        self.known_dependencies = known_dependencies
    
    def generic_visit(self, node):
        return ast.NodeVisitor.generic_visit(self, node)
    
    def visit_Name(self, node):
        if node.id in self.known_dependencies:
            self.found_dependencies.add(self.known_dependencies[node.id])


def find_known_dependencies_in_ast(target_ast: ast.AST, known_dependencies: dict[str, IntrafileDependecy]):
    visitor = KnownDependenciesCollector(known_dependencies)
    visitor.visit(target_ast)
    
    return visitor.found_dependencies


def ast_from_file(path):
    with open(path, 'r') as f:
        return ast.parse(f.read())


def ast_dump(tree):
    return ast.dump(tree,True,True,indent=4)


def pp_ast(tree):
    print(ast_dump(tree))
    
    
def set_ast_parent_labels(tree: ast.Module, preprocessor_state: PreprocessorState):
    for node in ast.walk(tree):
        setattr(node, 'has_body', hasattr(node, 'body'))
        
        for child in ast.iter_child_nodes(node):
            setattr(child, 'parent', node)
            setattr(child, 'has_body', hasattr(child, 'body'))
            
    return tree