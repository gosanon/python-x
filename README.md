# PythonX

[TBA, "Project overview" was here, but it's being replaced with a shorter description]

# Installation

1) Clone or download this repository (or just `release` folder)
2) Add `release` folder to PATH
3) Assuming your terminal has the folder in PATH, you are ready to go!

must also install "rich" library

# How to compile PythonX to Python

Create a file with `.x` extension (e.g. `test.x`). Code is written there.
To run the compiler and the program in the same process (for compilation only, change `xtender2.py` settings), run:
```
px test.x
```
# Linux support

Currently, "px" batch file is outdated and may not (mustn't) work as a global command. However,
it still works as a shortcut for `python xtender2.py`
The algorithm would be:
1) After download, run:
```
chmod +x px
```
2) and then:
```
px test.x
```
(but you should specify the absolute path to test.x or store it in the same directory)

# Configuration (not yet implemented)

Configuration can be done in two ways:
- console command arguments
- config file

1. For command line arguments info, use `px -h` or `px --help`
2. For example config file, have a look at `examples/px_example_config.py`. To pass the config file to
the compiler, use `px -c <path>` or `px --config <path>`.


# Project overview

This project was started because as a school student I wanted to start writing code even faster and make it more readable than before. We were only allowed to use basic tools and constructions, we couldn't even use pip on lessons, tests or contests (the code was being tested on remote servers where nobody would ever install any libraries for us). So, my solution was to deploy the tools I needed with a Preprocessor. I ended up with this architecture (described with some simplifications):

* functions and methods are stored in the main library file (`xcore.py`), should be in the same folder with the file where you write your code. Other (custom) methods or functions can be added there by users.

* the compilation (transpilation I'd say) starts when we call `python xtender2.py filename.x` command (there's a file extension checker which however can be removed). After that the transpiler is creating a new file `filename.py`. It tries to import (to validate) and parses `xcore.py` file by comments, to extract functions' and methods' definitions to insert some of those to final file.

* then the transpiler scans the file for comments and sting literals (this is done to skip tokenization process and make the python ast module do the job)

* after that's done, `xtender2.py` is preparing the file to be sent to python's `ast.parse`, it replaces all invalid syntax to python-valid, e.g.:
	- ```python
	  let { a, b } = obj
		```
		becomes

		```python
		( a, b, _pythonx_let) = obj
		```
		or
	- ```python
	  () => {}
		```
		becomes
		
		```python
		_pythonx_f0
		```
		and so on.

* then it is transforming the ast to finally make syntax features do the job:
	- e.g.

		```python
		( a, b, _pythonx_let) = obj
		```
		becomes

		```python
		(a, b) = map(lambda _px_t: getattr(obj, _px_t), ['a', 'b'])
		```

* analyzes functions' and methods' calls in the whole file (by walking through ast) and if it sees a name from `xcore.py` it adds it's definition to cache which will later printed to `filename.py`. Those rules can be controlled manually, add comments like this in the beginning of the `filename.x` file:
```python
# px-include-methods print_all
# px-exclude-methods filter, reduce
# px-include-functions p, inp
# px-exclude-functions o
```

* after, the ast is compiled back to source code via Python's `ast.unparse()` and it's goes straight to `filename.py`

* depending on settings, it may also run `autopep8` with `pycodestyle` afterwards

* finally, it tries to run `python filename.py` (in the same process). 

I've brought this into life and it worked perfectly for me. I saved a lot of time and learned some new things about Python and can now show you the latest version which offers stable functionality, more or less understandable or even familiar new syntax and maybe a few features that you'd like to take from my project and put into yours. So below comes a pretty detailed description of the apis available to use, but you are always free to ask questions!

# How to code in PythonX

As of now, PythonX is a superset of Python with a few new declarations and some syntax extensions. Below comes a short list of features:
## Functions:
- `inp()` for input, `p()` for output (`inp` differs from standard `input()`, `p` copies `print`)

- `o()` for anonymous objects (has special `args` handling)
## Syntax:
- `() => expr`, `() => { ... }` for arrow lambda functions

- `let { a, b } = expr` to define variables `a`, `b` equal to properties `a`, `b` of `expr`

- `o(a, b)` transforms to `o(a=a, b=b)` on compilation for js-like anonymous object syntax

- `px` class with method chaining, contains basic functional stuff like `map`, `reduce`, `filter` and some other methods. Methods are not lazy, though I am planning to add methods which return iterators instead of results of iteration.

- **(currently not implemented)** `$` - gets replaced with `lambda x:`

- **(currently not implemented)** `$.a, b, c:` - gets replaced with `lambda a, b, c:`

# `px` class

To be able to use PythonX methods, you need to convert your piece of data to a PythonX object:

`obj = px('Valera Borov 822')` or `obj = px([4, 8, 9, 1, 8, 37, 54, 13])` or even `obj = px(27)`.

You can pass an array of data to arguments if you want too.

Actually, you don't need a variable to solve some tasks. Instead, just make an object and run methods instantly: `px(132).sorted().print()` (output: `123`).
Also, the input function `i()` returns a PythonX object in most (not all of) cases (more info in **Functions**)

# Functions

`inp(arg (optional))` - an ultimate input function. Has one-letter kwarg `f` to read from custom istream.

* `inp()` - just like `input()`: read a line and return it.

* `inp(1)` or `i(True)`: same to `input().split()` but returns PythonX object.

* `inp(substr)`: same to `input().split(substr)` but returns PythonX object.

* `inp(func)`: same to `input().split()` and then `map()` on each of the splitted parts, returns PythonX object.

* `inp(int, func)`: read `int` lines and run `func` on each of them.

So, to split on multiple lines, you should do
* `px(range(int)).map($inp(1))`

*\* this function helped me on a contest when I didn't have my laptop with me. I just wrote it a few minutes before the start and later I could handle input in just one little line of code.*

`p(*args, **kwargs)` - behaves like common Python `print()` but has one (first) letter parameters. Uses sys.stdout. Used in `px.print()` and `px.print_all()` methods to print data from PythonX objects.

# Methods

If a method is not converting the PythonX object to other types, then it returns an updated version of the object allowing us to perform **method chaining** for any types of operations. Here is the full list of them:

Types

* `obj.to(new_type, (optional) joining)` - convert PythonX object to new_type by calling new_type(). If `joining`, will first join all parts as if they were symbols.

Printing

* `obj.print(s=sep, e=end)` - print the object and return it. `join` is used for `.type=str`

* `obj.print_all(s=sep, e=end)` - print the object using `*` and return it.

Passing functions into methods' chains (here you can execute any code on a PythonX object without a need to break the chain)

* `obj.call_safe(func)` - run `func(obj)`, then return the original `obj`. Use with procedure-like functions

* `obj.call(func)` - run `func(obj)`, then return the result of this call. Use with functions that return new useful values

Other methods

* `obj.len()` - you can now use `obj.len()` instead of `len(obj)` although both options are available.

* `obj.split((optional) split_sep)` - call `split()` for the object and return it's splitted version

* **(currently not implemented)** `obj.func_split(func_split_rule)` - splits by rule presented by a boolean function.

* `obj.map(func)` - run map on PythonX object, immidiately return result.

* `obj.sorted(k=key, r=reverse)` - return a sorted version of the object.

* `obj.filter(func(current_element))` - return a filtered version of the object. Uses standard `filter()`.

* **(currently not implemented)** `obj._filter(func(obj, current_element_index))` - a custom version of `filter` created to cover all usage cases including those which require index comparisons.

* **(currently not implemented)** `obj._while(cond, func, counter=lambda cntr: cntr + 1)` - a more compact version of `while` Python construction. A rare thing but can still be useful in some cases.

* `add(data)` - calls `obj += data`. So, can be used to put the data of `data` list to the end of `obj`.

# Old PythonX syntax additions
To work with Python's `ast`, I had to fully rewrite the `xtender.py`. Some inner APIs have remained available though I don't think I can rely on them so those syntax features were not implemented after the update.

P.S. `$`/`$.` operator might be implemented later but `~` is a postfix operator by nature and I haven't yet found a convenient way to make a "python-ast-valid" equivalent of it. 

- **(currently not implemented)** `$` Operator is used to simplify Python's default `lambda` syntax:
`$func(x)`, `$x.whatever()` is being transpiled to `lambda x:func(x)`, `lambda x:x.whatever()`. There are no variable detection algorithms, it's just `x` is the only variable by default. Such decision was made because `lambda` functions are naturally used with just one variable (`map`, `filter`, etc.)

- **(currently not implemented)** `$.a, b, c: whatever(a, b, c)` is being transpiled to `lambda a, b, c: whatever(a, b, c)`. So in case if you need any other variables or just want to change the name of one required, `$.` prefix is used.

- **(currently not implemented)** `~` Operator is a quite different thing. To cut things short, we can now call `_()` by simply adding `~` in the end of a construction. Just some examples:
	* `'some str'~` becomes `_('some str')`;

	* `[ my_var ]~, another_one~` becomes `_([ my_var ]), _(another_one)`;

	* `func(anything)~` becomes `_(func(anything))` and so on. That means we can jump from one type to another in a few symbols, e.g.:

	`sumOf132 = 132~.map(int).call(sum).to(int, true)` will simply and clearly calculate the sum of digits of 132 and put the result into the variable. The result has an `int` type, of course.

So, that is all that was done so far. I don't think this project can be expanded or monetized in any way, so I will only keep polishing it from time to time, hoping for newcomers to get involved in it.

Have fun and good luck!

Cordially yours,
Zaq
